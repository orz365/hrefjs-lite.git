const Hrefjs = require('../dist/hrefjs-lite.min.js')
// import Hrefjs from "hrefjs-lite";
const url = 'https://www.cssjs.cn:92/tt/bb/#/fly?a=10&a=20&a=30'
const location = new Hrefjs(url)

// 增加参数
location.searchParams.set('name', 'Tom')
console.log(location.toString())
// https://www.cssjs.cn:92/tt/bb/?name=Tom#/fly?a=10&a=20&a=30

// 增加hash的参数信息
location.hashSearchParams.set('age', 40)
console.log(location.toString())
// https://www.cssjs.cn:92/tt/bb/?name=Tom#/fly?a=10&a=20&a=30&age=40

// 修改path路径
location.pathname = '/new/path'
console.log(location.toString())
// https://www.cssjs.cn:92/new/path?name=Tom#/fly?a=10&a=20&a=30&age=40

// 修改hash路径
location.hashPath = '#/new/path'
location.searchParams.clear()
console.log(location.hashSearchParams)
location.hashSearchParams.clear()
console.log(location.hashSearchParams)
console.log(location.toString())
// https://www.cssjs.cn:92/new/path?name=Tom#/new/path?a=10&a=20&a=30&age=40
