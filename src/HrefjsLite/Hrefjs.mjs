import SearchParams from './SearchParams.mjs'
import urlUtils from './url.mjs'

/**
 * 拆分hash参数
 */
function slitHashSearch() {
  const hashPathSearch = this.hash.split('?')
  const hashPath = hashPathSearch[0] || ''
  const hashSearch = hashPathSearch[1] || ''

  // 不是以#/开头，以#号开头
  this.hashPath = hashPath.replace(/#\/?()/, '/$1')

  this.hashSearch = hashSearch
  if (hashSearch) {
    this.hashSearchParams = new SearchParams(hashSearch)
  } else {
    this.hashSearchParams = new SearchParams()
  }
  this.hashSearchJson = this.hashSearchParams
}

/**
 * 扩展URL
 */
export default class Hrefjs {
  constructor(url) {
    let temp = new URL(url)
    let location = {
      href: temp.href,
      origin: temp.origin,
      protocol: temp.protocol,
      username: temp.username,
      password: temp.password,
      host: temp.host,
      hostname: temp.hostname,
      port: temp.port,
      pathname: temp.pathname,
      search: temp.search,
      searchParams: new SearchParams(temp.searchParams),
      hash: temp.hash
    }
    slitHashSearch.apply(location)
    Object.assign(this, location)
    return new Proxy(this, {
      get(obj, prop) {
        let value = obj[prop]
        if (prop === 'hashPath') {
          value = value.replace(/#\/?()/, '/$1')
        }
        return value
      }
    })
  }
  toString() {
    return urlUtils.revert(this)
  }
  static json2param(json) {
    return urlUtils.json2param(json)
  }
  static param2json(param) {
    return urlUtils.param2json(param)
  }
}
