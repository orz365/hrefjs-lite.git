export default class SearchParams extends URLSearchParams {
  constructor(params) {
    super(params)
  }
  /**
   * 将{a:10}类型的数据，还原成参数 a=10&b=20
   * 和 toString方法一样
   */
  toParam() {
    let param = []
    this.forEach((value, name) => {
      param.push(`${name}=${encodeURIComponent(value)}`)
    })
    return param.join('&')
  }
  toJson() {
    let searchParams = {}
    this.forEach((value, name) => {
      name = decodeURIComponent(name)
      value = decodeURIComponent(value)
      if (!searchParams[name]) {
        searchParams[name] = value
      } else if (Array.isArray(searchParams[name])) {
        searchParams[name].push(value)
      } else {
        searchParams[name] = [searchParams[name], value]
      }
    })
    return searchParams
  }
  /**
   * 清空参数
   */
  clear() {
    let keys = [...this.keys()]
    for (let key of keys) {
      this.delete(key)
    }
  }
}
