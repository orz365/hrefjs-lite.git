# hrefjs-lite

根据提供的href地址链接，解析出location结构数据，
参数与json互转，hash参数解析转换。

### 安装

```bash
npm i hrefjs-lite
```

### 使用

```js
import Hrefjs from "hrefjs-lite"
const url = 'https://www.cssjs.cn:92/tt/bb/#/fly?a=10&a=20&a=30'
const location = new Hrefjs(url)
console.log(location)
// 返回结果
{
  href: 'https://www.cssjs.cn:92/tt/bb/#/fly?a=10&a=20&a=30',
  origin: 'https://www.cssjs.cn:92',
  protocol: 'https:',
  username: '',
  password: '',
  host: 'www.cssjs.cn:92',
  hostname: 'www.cssjs.cn',
  port: '92',
  pathname: '/tt/bb/',
  search: '',
  searchParams: r {},
  hash: '#/fly?a=10&a=20&a=30',
  hashPath: '/fly',
  hashSearch: 'a=10&a=20&a=30',
  hashSearchParams: r { 'a' => '10', 'a' => '20', 'a' => '30' },
  hashSearchJson: r { 'a' => '10', 'a' => '20', 'a' => '30' }
}
```

```js
import Hrefjs from 'hrefjs-lite'
var json = {
  a: 10,
  b: 20
}
var param = Hrefjs.json2param(json)
console.log(param)
// a=10&b=20

param = 'a=10&b=20'
json = Hrefjs.param2json(param)
console.log(json)
//{ a: '10', b: '20' }
```

### 修改URL参数和hash参数

```js
import Hrefjs from 'hrefjs-lite'
const url = 'https://www.cssjs.cn:92/tt/bb/#/fly?a=10&a=20&a=30'
const location = new Hrefjs(url)

// 增加参数
location.searchParams.set('name', 'Tom')
console.log(location.toString())
// https://www.cssjs.cn:92/tt/bb/?name=Tom#/fly?a=10&a=20&a=30

// 增加hash的参数信息
location.hashSearchParams.set('age', 40)
console.log(location.toString())
// https://www.cssjs.cn:92/tt/bb/?name=Tom#/fly?a=10&a=20&a=30&age=40

// 修改path路径
location.pathname = '/new/path'
console.log(location.toString())
// https://www.cssjs.cn:92/new/path?name=Tom#/fly?a=10&a=20&a=30&age=40

// 修改hash路径
location.hashPath = '/new/path'
console.log(location.toString())
// https://www.cssjs.cn:92/tt/bb/?name=Tom#/new/path?a=10&a=20&a=30&age=40
```
